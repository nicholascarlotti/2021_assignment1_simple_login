package it.assignment1.simplelogin;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
public class HashGeneratorTest {
    @Test
    public void testGetHashedString(){
        String toHash = "this is a string";
        String hashed = HashGenerator.getHashedString(toHash);
        assertFalse(toHash.compareTo(hashed) == 0);
    }
}
