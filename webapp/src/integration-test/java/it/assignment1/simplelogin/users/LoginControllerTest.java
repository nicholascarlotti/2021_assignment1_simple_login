package it.assignment1.simplelogin.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.MvcResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.Assert;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import it.assignment1.simplelogin.HashGenerator;
import it.assignment1.simplelogin.users.login.LoginController;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public class LoginControllerTest {
    @Container
    private static final PostgreSQLContainer<?> POSTGRESQL_CONTAINER = new PostgreSQLContainer<>("postgres:14-alpine")
            .withDatabaseName("integration-tests-db").withUsername("admin").withPassword("password");

    @Autowired
    private LoginController loginController;

    @Autowired
    private UserRepository userRepository;

    private User defaultUser;

    @Autowired
    private MockMvc mockMvc;

    private static final String USER_PASSWORD = "password";

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword);
    }

    @AfterEach
    void cleanUp() {
        this.userRepository.deleteAll();
    }

    @BeforeEach
    void setUp() {
        String username = "username";
        this.defaultUser = this.userRepository.save(new User(username, HashGenerator.getHashedString(USER_PASSWORD)));
    }

    @Test
    void testLoginSuccessful() throws Exception {
        MvcResult request = this.mockMvc.perform(
                post("/login").param("username", this.defaultUser.getUsername()).param("password", USER_PASSWORD))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    void testLoginWrongPassword() throws Exception {
        this.mockMvc.perform(
                post("/login").param("username", this.defaultUser.getUsername()).param("password", "INVALID_PASSWORD"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testLoginUserDoesNotExists() throws Exception {
        this.mockMvc.perform(
                post("/login").param("username", "NON_EXISTING").param("password", USER_PASSWORD))
                .andExpect(status().isUnauthorized());
    }

}
