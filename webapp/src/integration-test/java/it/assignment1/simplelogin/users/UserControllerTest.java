package it.assignment1.simplelogin.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.MvcResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.Assert;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import it.assignment1.simplelogin.HashGenerator;
import it.assignment1.simplelogin.users.*;
import it.assignment1.simplelogin.users.login.*;
import java.util.List;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public class UserControllerTest {
    @Container
    private static final PostgreSQLContainer<?> POSTGRESQL_CONTAINER = new PostgreSQLContainer<>("postgres:14-alpine")
            .withDatabaseName("integration-tests-db").withUsername("admin").withPassword("password");

    @Autowired
    private LoginController loginController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword);
    }

    @AfterEach
    void cleanUp() {
        this.userRepository.deleteAll();
    }

    @Test
    public void registerUserTest() throws Exception{
        String username = "tizio";
        String password = "p4caio";
        this.mockMvc.perform(
                post("/users").param("username", username).param("password", password))
                .andExpect(status().isCreated());
        List<User> byUsername = this.userRepository.findByUsername(username);
        Assert.assertTrue(byUsername.size() == 1);
        User user = byUsername.get(0);
        Assert.assertTrue(user.getUsername().compareTo(username) == 0);
        Assert.assertTrue(user.getPassword().compareTo(password) != 0);
    }
    @Test
    public void registerDuplicateUserTest() throws Exception{
        String username = "tizio";
        String password = "pwd";
        User newUser = this.userRepository.save(new User(username, HashGenerator.getHashedString(password)));
        this.mockMvc.perform(
                post("/users").param("username", newUser.getUsername()).param("password", password))
                .andExpect(status().isBadRequest());
    }
}