package it.assignment1.simplelogin.users;

import static org.assertj.core.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Testcontainers
class UserRepositoryIntegrationTest {
    @Container
	private static final PostgreSQLContainer<?> POSTGRESQL_CONTAINER = new PostgreSQLContainer<>("postgres:14-alpine")
        .withDatabaseName("integration-tests-db")
        .withUsername("admin")
        .withPassword("password");

    @Autowired
    private UserRepository userRepository;

    @DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry registry) {
		registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword);
	}

	@AfterEach
	void cleanUp() {
		this.userRepository.deleteAll();
	}

    @Test
    void shouldSaveAndLoadAUser() {
        User persistedUser = this.userRepository.save(new User("VincenzoDrift", "1234"));
        Optional<User> retrievedUser = this.userRepository.findById(persistedUser.getId());

        assertThat(retrievedUser).isNotEmpty();
        assertThat(this.userRepository.count()).isEqualTo(1);
        assertThat(retrievedUser.get()).usingRecursiveComparison().isEqualTo(persistedUser);
    }
}
