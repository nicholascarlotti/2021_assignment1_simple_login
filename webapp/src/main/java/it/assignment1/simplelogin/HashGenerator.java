package it.assignment1.simplelogin;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashGenerator {
    public static String getHashedString(String password){
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        byte [] encodedHash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder hexString = new StringBuilder(encodedHash.length * 2);
        for(int i = 0; i < encodedHash.length; i++){
            String hex = Integer.toHexString(encodedHash[i]);
            if(hex.length() == 1){
                hexString.append(0);
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
