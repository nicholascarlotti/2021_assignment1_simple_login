package it.assignment1.simplelogin;
import org.springframework.web.bind.annotation.*;
import it.assignment1.simplelogin.users.exceptions.DuplicateUsernameException;
import it.assignment1.simplelogin.users.exceptions.InvalidPasswordException;
import it.assignment1.simplelogin.users.exceptions.InvalidUserException;

import org.springframework.http.HttpStatus;

@RestControllerAdvice
public class RestErrorHandler {

        @ExceptionHandler(DuplicateUsernameException.class)
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public DuplicateUsernameException handleDuplicateUsernameException(DuplicateUsernameException ex){
            return ex;
        }

        @ExceptionHandler(InvalidPasswordException.class)
        @ResponseStatus(HttpStatus.UNAUTHORIZED)
        public void handleInvalidPassException(InvalidPasswordException ex) {

        }

        @ExceptionHandler(InvalidUserException.class)
        @ResponseStatus(HttpStatus.UNAUTHORIZED)
        public void handleInvalidUserException(InvalidUserException ex) {

        }
}
