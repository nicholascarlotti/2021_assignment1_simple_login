package it.assignment1.simplelogin.users.login;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import it.assignment1.simplelogin.HashGenerator;
import it.assignment1.simplelogin.users.User;
import it.assignment1.simplelogin.users.UserController;
import it.assignment1.simplelogin.users.UserRepository;
import it.assignment1.simplelogin.users.exceptions.InvalidPasswordException;
import it.assignment1.simplelogin.users.exceptions.InvalidUserException;
import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
@RequestMapping("/login")
public class LoginController{
    private UserRepository userRepository;

    @GetMapping
    public String getLoginPage(){
        return "login";
    }

    @PostMapping
    public String doLogin(@RequestParam("username") String username, @RequestParam("password") String password, ModelMap model){
        String result = "";
        String hashedPwd = HashGenerator.getHashedString(password);
        List<User> userList = this.userRepository.findByUsername(username);
        if(userList.size() == 0){
            model.addAttribute("errorMessage", "User does not exist");
            result = "loginError";
            throw new InvalidUserException();
        }else{
            User user = userList.get(0);
            if(hashedPwd.compareTo(user.getPassword()) == 0){
                model.addAttribute("username", user.getUsername());
                result = "home";
            }else{
                model.addAttribute("errorMessage", "Invalid password");
                result = "loginError";
                throw new InvalidPasswordException();
            }
        }
        return result;


    }
}