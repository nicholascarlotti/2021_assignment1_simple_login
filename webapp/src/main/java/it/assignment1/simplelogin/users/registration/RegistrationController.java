package it.assignment1.simplelogin.users.registration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    @GetMapping
    public String getRegistrationPage(){
        return "registration";
    }
}
