package it.assignment1.simplelogin.users;

import java.util.List;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.http.HttpStatus;

import it.assignment1.simplelogin.HashGenerator;
import it.assignment1.simplelogin.users.exceptions.DuplicateUsernameException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {
    private UserRepository userRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestParam("username") String username, @RequestParam("password") String password) throws DuplicateUsernameException{
            User result;
            if(this.userRepository.findByUsername(username).size() > 0){
                throw new DuplicateUsernameException();
            }else{
                result = this.userRepository.save(new User(username, HashGenerator.getHashedString(password)));
            }
            return result;
    }

}
