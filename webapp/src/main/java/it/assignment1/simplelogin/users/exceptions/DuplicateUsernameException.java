package it.assignment1.simplelogin.users.exceptions;

/**
 * DuplicateUsernameException
 */
public class DuplicateUsernameException extends RuntimeException{

    private String errorMessage;

    public DuplicateUsernameException(String errorString){
        this.errorMessage = errorString;
    }

    public DuplicateUsernameException(){
        this("The username provided already exists");
    }
}