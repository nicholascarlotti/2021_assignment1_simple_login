<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Test: hello</title>
    <style>
      a {
        -webkit-appearance: button;
        -moz-appearance: button;
        appearance: button;
        border-width: 1px;
        border-radius: 5px;
        border-color: black;
        border-style: solid;
        padding: .5rem;
        text-decoration: none;
        color: initial;
      }
    </style>
  </head>

  <body>
    <div
      style="
        text-align: center;
        height: 100vh;
        display: flex;
        justify-content: center;
        align-content: center;
        flex-wrap: wrap;
      "
    >
    <h1 style="width: 100%;">Simple Login</h1>
      <a style="margin-right: 1rem; padding: 0.5rem" href="/login">Login</a>
      <a style="margin-left: 1rem; padding: 0.5rem" href="/registration"
        >Registrazione</a
      >
    </div>
  </body>
</html>
