<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Registration</title>
  </head>
  <script>
    async function handleOnSubmit() {
      const username = document.getElementById("username-input").value;
      const pwdInput = document.getElementById("password-input");
      const pwd = pwdInput.value;
      const pwdConfirmationInput = document.getElementById(
        "password-confirmation-input"
      );
      const pwdConf = pwdConfirmationInput.value;
      let error = false;
      if (username.length < 5) {
        document.getElementById("error-paragraph").innerHTML =
          "Username is too short, 5 characters minimum";
          error = true;
      }
      if (!pwd) {
        document.getElementById("error-paragraph").innerHTML =
          "Password field cannot be empty";
          error = true;
      }
      if (pwd !== pwdConf) {
        document.getElementById("error-paragraph").innerHTML =
          "Passwords do not match";
          error = true;
      }
      if(!error){
        const formData = new FormData();
        formData.append('username', username);
        formData.append('password', pwd);
        const res = await fetch("/users", {
          method: "POST",
          body: formData,
          
        });
        window.location.href = "/login";
      }
      return false;
    }
  </script>
  <body>
    <form
    style="display: flex; flex-direction: column; width: 20rem; margin-right: auto; margin-left: auto; justify-content: center;"
    onsubmit="handleOnSubmit(); return false;"
      method="post"
      id="registration-form"
      action="/users"
    >
      <label for="username-input">Username</label>
      <input type="text" name="username" id="username-input" />
      <label for="password-input">Password</label>
      <input type="password" name="password" id="password-input" />
      <label for="password-confirmation-input">Password confirmation</label>
      <input
        type="password"
        name="password-confirmation"
        id="password-confirmation-input"
      />
      <input type="submit" value="Login" />
    </form>
    <p id="error-paragraph"></p>
  </body>
</html>
