<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Login</title>
  </head>
  
  <style>
    form > * {
      margin-bottom: .5rem;
    }
  </style>
  <body>
    <form method="post" style="display: flex; flex-direction: column; margin-right: auto; margin-left: auto; margin-top: 10rem; width: 20rem;">
      <label for="username">Username</label>
      <input type="text" name="username" id="username"/>
      <label for="password">Password</label>
      <input type="password" name="password" id="password">
      <input type="submit" value="Login">
    </form>
  </body>
</html>
