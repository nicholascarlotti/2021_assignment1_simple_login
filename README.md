# 2021 Assignment1 Simple Login
[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=nicholascarlotti_2021_assignment1_simple_login&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=nicholascarlotti_2021_assignment1_simple_login&metric=bugs)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=nicholascarlotti_2021_assignment1_simple_login&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=nicholascarlotti_2021_assignment1_simple_login&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=nicholascarlotti_2021_assignment1_simple_login&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nicholascarlotti_2021_assignment1_simple_login&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=nicholascarlotti_2021_assignment1_simple_login)

## Componenti del gruppo
- Carlotti Nicholas `883229`
- Corso Vincenzo `880965`
  
## Links
Repository GitLab: [https://gitlab.com/nicholascarlotti/2021_assignment1_simple_login]()

Repository SonarCloud: [https://sonarcloud.io/project/overview?id=nicholascarlotti_2021_assignment1_simple_login]()

## Introduzione
Questa repository contiene una basilare webapp che permette di registrare un account e di effettuare il login.
Ci sono tre routes:
- `/` permette di scegliere se effettuare la registrazione o il login
- `/registration` permette di registrare un nuovo account
- `/login` permette di effettuare l'accesso con un account precedentemente registrato

La webapp è stata realizzata utilizzando il framework Spring. Come build tool si è deciso di utilizzare Gradle v7 poiché, grazie ai suoi numerosi plugin, permette una elevata configurabilità in modo molto semplice.

## Nota
Per semplicità si è deciso di ignorare gli aspetti relativi alla sicurezza, dato che l'applicazione non è l'obiettivo principale dell'assignment.

## Pipeline
La pipeline realizzata prevede 7 stage:
- [x] Build
- [x] Verify
- [x] Test
- [x] Integration Test
- [x] Package
- [x] Release
- [x] Deploy

### Stages

Lo stage **Build** compila l'applicazione, verificando se ci siano degli errori. 

Lo stage **Verify** prevede due job. Il primo utilizza *Checkstyle* per eseguire il linting del progetto ed assicurarsi che il codice rispetti alcune convenzioni stilistiche. Il secondo esegue una analisi statica del codice ed invia i risultati su *SonarCloud*. Si è deciso di utilizzare questo tool perché è possibile definire, gestire e configurare un proprio *Quality Gate*.

Lo stage **Test** esegue tutti gli unit test contenuti nella cartella `webapp/src/test`. Il framework di testing utilizzato è *JUnit5*.

Lo stage **Integration Test** esegue tutti i test di integrazione contenuti nella cartella `webapp/src/integration-test`. La separazione tra le due tipologie di test è stata ottenuta mediante un plugin gradle (vedi `webapp/build.gradle`), il quale permette di gestire più *test sets*. In particolare viene utilizzata la libreria [Test containers](https://www.testcontainers.org/) per testare la collaborazione tra la webapp e il database. Quest'ultima permette di dichiarare programmaticamente dei container docker, in modo da poter testare semplicemente la collaborazione tra più componenti. Per avviare questo task è pertanto necessario avere *Docker* installato sulla propria macchina.

Lo stage **Package** crea un file jar eseguibile, contenente una versione embedded di *Tomcat* e l'applicazione con le relative dipendenze.

Nello stage di **Release** viene fatto uso del plugin Gradle *JIB* in modo da creare un'immagine *Docker* ed effetturane il push sul container registry di *GitLab*. *JIB* è un plugin sviluppato da *Google* che permette di automatizzare la costruzione e la distribuzione di immagini Docker in ambiente *Java*. Il tag associato all'immagine corrisponde al nome del branch su cui è stato eseguito il job. Per tale motivo l'immagine con tag `main` sarà usata in produzione, in quanto tale branch contiene le versioni dell'applicazione idonee all'essere messe in produzione.

Lo stage **Deploy** fa uso della piattaforma cloud [*Okteto*](https://okteto.com/), che permette di distribuire applicazioni containerizzate attraverso file di specifica *Docker Compose*, *Kubernetes* oppure proprietari. Nel nostro caso abbiamo deciso di specificare la configurazione dell'applicazione attraverso la specifica proprietaria di Okteto che consiste in un superset delle specifiche *Docker Compose* (vedi `okteto-stack.yaml`).
Nell'ottica di uno sviluppo multi-branch, questo stage della pipeline verrà eseguito esclusivamente sul branch `main`.

### Webapp
La webapp distribuita durante lo stage di deploy è disponibile al seguente [link](https://webapp-vincenzocorso.cloud.okteto.net/).
